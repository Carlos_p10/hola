import Foundation

var pizaaInInches : Int = 10 {
    willSet{
        print(newValue)
    } //estos son monitores de cuando pizza inches cambia
    didSet {
        if pizaaInInches >= 18 {
            print("Tamano invalido porque es 18")
            pizaaInInches = 18
        }
    }
}

pizaaInInches = 19
print(pizaaInInches)

var numberOfPeople : Int = 12
let slicesPorPerson: Int = 4

var numberOfSlices : Int{
    get {
        return pizaaInInches - 4
    }
    set {
        print("El nuevo valor es: \(newValue)")
    }
}

var numberOfPizza: Int {
    get {
        let numberOfPeopleFedPerPizza = numberOfSlices / slicesPorPerson
        return numberOfPeople / numberOfPeopleFedPerPizza
    }
    set{
        let tot = numberOfSlices * newValue
        numberOfPeople = tot / slicesPorPerson
    }
}
numberOfPizza = 3

print(numberOfPeople)
