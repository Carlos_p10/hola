//
//  ViewController.swift
//  SeeFood
//
//  Created by Carlos Paredes on 2/21/20.
//  Copyright © 2020 Carlos Paredes. All rights reserved.
//

import UIKit
import CoreML
import Vision

class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate{

    @IBOutlet weak var imageView: UIImageView!

    let imagePicker = UIImagePickerController()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imagePicker.delegate = self
        //imagePicker.sourceType = .photoLibrary //Esto es para acceder a la libreria en proximas
        imagePicker.sourceType = .camera
        imagePicker.allowsEditing = false
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let userImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
            imageView.image = userImage
            
            guard let ciimage = CIImage(image: userImage) else {
                fatalError("Cant convert too ciimage, sorry bro")
            }
            
            detect(image: ciimage)
            
        }
        
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    func detect(image : CIImage){
        
        guard let model = try? VNCoreMLModel(for: Inceptionv3().model) else {
            fatalError("Loading CoreML Failed")
        }
        
        let request = VNCoreMLRequest(model: model) { (request, error) in
            guard let resutls = request.results as? [VNClassificationObservation] else {
                fatalError("El resultado no se pudo obtener del Model")
            }
            
            if let firstResult = resutls.first {
                if firstResult.identifier.contains("hotdog"){
                    self.navigationItem.title = "Hotdog! xD"
                }else {
                    self.navigationItem.title = "No es Hotdog! :("
                }
            }
        }
        
        let handler = VNImageRequestHandler(ciImage: image)
        do {
            try handler.perform([request])
        } catch {
            print(error)
        }
        
    }
    
    @IBAction func cameraTapped(_ sender: UIBarButtonItem) {
        
        present(imagePicker, animated: true, completion: nil)
    }

}

