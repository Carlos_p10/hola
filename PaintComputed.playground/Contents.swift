import Foundation

var width: Float = 3.4
var height: Float = 2.1

var bucketsOfPaint : Int {
    get {
        let area = width * height
        let areaCoveredPerBucket : Float = 1.5
        let numberOfBuckets = area / areaCoveredPerBucket
        let roundedBuckets = ceilf(numberOfBuckets)
        return Int(roundedBuckets)
    }
    set{
        let areaCanCovered = Double(newValue) * 1.5
        print("Esta cantidad puede ser cubierta \(areaCanCovered)")
    }
}


bucketsOfPaint = 8

