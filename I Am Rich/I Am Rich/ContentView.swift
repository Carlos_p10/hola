//
//  ContentView.swift
//  I Am Rich
//
//  Created by Carlos Paredes on 2/12/20.
//  Copyright © 2020 Carlos Paredes. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        ZStack{
            Color(.systemTeal).edgesIgnoringSafeArea(.all)
            VStack {
                Text("I Am Rich")
                    .font(.system(size: 40))
                    .fontWeight(.bold)
                    .foregroundColor(Color.white)
                
                Image("diamond")
                    .resizable() //Hace que la imagen encaje
                    .aspectRatio(contentMode: .fit) //Hace que se ajuste mas la imagen
                    .frame(width: 200, height: 200, alignment: .center) //Se define la altura y ancho de la imagen
            }
            
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
