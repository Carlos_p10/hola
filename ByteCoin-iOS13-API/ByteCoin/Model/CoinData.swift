//
//  CoinData.swift
//  ByteCoin
//
//  Created by Carlos Paredes on 2/10/20.
//  Copyright © 2020 The App Brewery. All rights reserved.
//

import Foundation

struct CoinData: Decodable {
    //let asset_id_quote: String
    let rate: Double
    
}
