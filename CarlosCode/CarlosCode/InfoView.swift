//
//  InfoView.swift
//  CarlosCode
//
//  Created by Carlos Paredes on 2/12/20.
//  Copyright © 2020 Carlos Paredes. All rights reserved.
//

import SwiftUI

struct InfoView: View {
    
    let text: String
    let imageName: String
    
    var body: some View {
        RoundedRectangle(cornerRadius: 25)
            .fill(Color.white)
            .frame(height: 50.0)
            .overlay(HStack {
                Image(systemName: imageName) //escoge unas imagenes como iconos ya definidas dentro de apple
                    .foregroundColor(.green)
                
                Text(text)
            })
            .padding(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/)
    }
}


struct InfoView_Previews: PreviewProvider {
    static var previews: some View {
        InfoView(text: "hola", imageName: "phone.fill")
            .previewLayout(.sizeThatFits)
    }
}
