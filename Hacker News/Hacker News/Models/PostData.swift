//
//  PostData.swift
//  Hacker News
//
//  Created by Carlos Paredes on 2/12/20.
//  Copyright © 2020 Carlos Paredes. All rights reserved.
//

import Foundation


struct Results: Decodable {
    let hits: [Hits]
}

struct Hits: Decodable, Identifiable {
    var id: String{ return objectID } //Esta variable es necesaria y se debe de llamar de esta manera por Identifiable
    let objectID: String
    let points: Int
    let title: String
    let url: String?
}
