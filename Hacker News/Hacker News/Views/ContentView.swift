//
//  ContentView.swift
//  Hacker News
//
//  Created by Carlos Paredes on 2/12/20.
//  Copyright © 2020 Carlos Paredes. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    
    @ObservedObject var networkManager = NetworkManager()
    
    var body: some View {
        NavigationView{ //Funcionara para navegar entre las pilas de las ventanas abiertas
            List(networkManager.posts){ post in
                NavigationLink(destination: DetailView(url: post.url)) {
                    HStack {
                        Text(String(post.points))
                        Text(post.title)
                    }
                }
            } //para cada post dentro posts utilizare el texto para identificar
                .navigationBarTitle("HACKER NEWS")
        }
        .onAppear {
            self.networkManager.fetchData()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

//let posts = [
//    Post(id: "1", title: "Hello"),
//    Post(id: "2", title: "Helfflo"),
//    Post(id: "3", title: "Heooollo"),
//]
